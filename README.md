# Laravel Admin Panel
An admin panel for managing users, roles, permissions & crud.

### Requirements
    Laravel >=9
    PHP >= 8.0
    composer `2.4.2`
    node `16.17.1`
    npm `8.15.0`
    WebPack and laravel mix
    Now we Support Laravel 9


### templates version 


Bootstrap `v4`

AdminLTE `3.1`
## Features
- User, Role & Permission Manager
- CRUD Generator
- Activity Log
- Page CRUD
- Settings

## Installation

1. Run
    ```
    composer require acmepackage/laravel-admin
    ```
2. Make sure your user model's has a ```HasRoles``` trait **app/Models/User.php**.
    ```php
    class User extends Authenticatable
    {
        use Notifiable, HasRoles;

        ...
    ```
3. Create auth assets  
     ```
     php artisan ui bootstrap --auth  
    ```
     and don't replace auth views
4. Add the following line to the "web.php" file located in routes folder
``````
    require('admin.php');
```````

5. Register localization middleware in app/Http/Kernel.php

````
protected $middlewareGroups = [
'web' => [

            \App\Http\Middleware\Localization::class,

        ],

    ];
````
6. Run the following command
```angular2html
php artisan vendor:publish --all
```
7. Remove your package.json located in the project root dir and rename package.json.example in the same dir to  package.json
   then rename webpack.mix.js.example to webpack.mix.js

8. Run the following commands to install node and build the node packages
``````
    npm install
``````
``````
    npm install --save-dev webpack
``````
 ``````
     npm run dev
```````
9. To create seeder for roles and language, add the following lines to DatabaseSeeder.php in the "run" function
``````
     $this->call([
             RoleTableSeeder::class,
            LanguageTableSeeder::class,
        ]);
``````

9. Finally run
  `````
     php artisan migrate --seed
``````
10. Open the following url after running "php artisan serve"
    your_url/admin
  
 
Note: we use webpack only not vite 

Note: If you are using Laravel 7+ then scaffold the authentication with bootstrap for a better experience.
. You can generate CRUD easily through generator tool now.

## Usage

1. Create some permissions.

2. Create some roles.

3. Assign permission(s) to role.

4. Create user(s) with role.

5. For checking authenticated user's role see below:
    ```php
    // Add role middleware in app/Http/Kernel.php
    protected $routeMiddleware = [
        ...
        'role' => \App\Http\Middleware\CheckRole::class,
    ];
    ```

    ```php
    // Check role anywhere
    if (Auth::check() && Auth::user()->hasRole('admin')) {
        // Do admin stuff here
    } else {
        // Do nothing
    }

    // Check role in route middleware
    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['auth', 'role:admin']], function () {
       Route::get('/', ['uses' => 'AdminController@index']);
    });

    // Check permission in route middleware
    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['auth', 'can:write_user']], function () {
       Route::get('/', ['uses' => 'AdminController@index']);
    });
    ```

6. For checking permissions see below:

    ```php
    if ($user->can('permission-name')) {
        // Do something
    }
    ```

Learn more about ACL from [here](https://laravel.com/docs/master/authorization)

For activity log please read `spatie/laravel-activitylog` [docs](https://docs.spatie.be/laravel-activitylog/v2/introduction)

## Screenshots

![users](https://user-images.githubusercontent.com/1708683/43477093-1ac08d42-951c-11e8-8217-00aedc19b28d.png)

![activity log](https://user-images.githubusercontent.com/1708683/43477154-426d849e-951c-11e8-8682-ac1950114a5a.png)

![generator](https://user-images.githubusercontent.com/1708683/43477174-5381d15e-951c-11e8-9f86-2e45acd38f08.png)

![settings](https://user-images.githubusercontent.com/1708683/43679408-67b724d0-9846-11e8-8eb0-49e04c449ee3.png)

## Author

[Mohamed Hassan] :email: [Email Me](mailto:mohamedhassan225588@gmail.com)

##reference

[Sohel Amin](http://www.sohelamin.com) :email: [Email Me](mailto:sohelamincse@gmail.com)

## License

This project is licensed under the MIT License - see the [License File](LICENSE) for details
