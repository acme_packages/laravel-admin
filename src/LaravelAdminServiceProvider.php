<?php

namespace acmepackage\LaravelAdmin;

use File;
use Illuminate\Support\ServiceProvider;

class LaravelAdminServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->publishes([
            __DIR__ . '/../publish/Middleware/' => app_path('Http/Middleware'),
        ]);

        $this->publishes([
            __DIR__ . '/../publish/migrations/' => database_path('migrations'),
        ]);
        $this->publishes([
            __DIR__ . '/../publish/seeders/' => database_path('seeders'),
        ]);

        $this->publishes([
            __DIR__ . '/../publish/Models/' => app_path('Models'),
        ]);

        $this->publishes([
            __DIR__ . '/../publish/Controllers/' => app_path('Http/Controllers'),
        ]);
        $this->publishes([
            __DIR__ . '/../publish/Helpers/' => app_path('Helpers'),
        ]);
        $this->publishes([
            __DIR__ . '/../publish/Traits/' => app_path('Traits'),
        ]);
        $this->publishes([
            __DIR__ . '/../publish/lang/' => base_path('lang'),
        ]);
        $this->publishes([
            __DIR__ . '/../publish/js/' => base_path(),
        ]);
        $this->publishes([
            __DIR__ . '/../publish/routes/' => base_path('routes'),
        ]);
        $this->publishes([
            __DIR__ . '/../publish/public/' => base_path('public'),
        ]);

        $this->publishes([
            __DIR__ . '/../publish/resources/' => base_path('resources'),
        ]);

        $this->publishes([
            __DIR__ . '/../publish/crudgenerator.php' => config_path('crudgenerator.php'),
        ]);

        $this->publishes([
            __DIR__ . '/views' => base_path('resources/views/vendor/laravel-admin'),
        ], 'views');

        $this->loadViewsFrom(__DIR__ . '/views', 'laravel-admin');

        $menus = [];
        if (File::exists(base_path('resources/laravel-admin/menus.json'))) {
            $menus = json_decode(File::get(base_path('resources/laravel-admin/menus.json')));
            view()->share('laravelAdminMenus', $menus);
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands(
            'acmepackage\LaravelAdmin\LaravelAdminCommand'
        );

        $this->app->bind('Setting', \acmepackage\LaravelAdmin\Setting::class);
    }
}
